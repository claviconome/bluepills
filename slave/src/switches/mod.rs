/*
 * File: mod.rs
 * Project: switches
 * Created Date: Thursday February 4th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 23rd February 2021 10:16:24 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

pub mod matrix;
pub mod debouncer;

pub use matrix::{Matrix};
pub use debouncer::{Debouncer, DebounceResult};