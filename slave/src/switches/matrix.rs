

use stm32f1xx_hal::gpio::{ErasedPin, Input, OpenDrain, Output, PullUp};
use cortex_m;
// use embedded_hal::digital::v2::{InputPin, OutputPin};
use no_std_compat::prelude::v1::*;
use smallbitvec::SmallBitVec;
use crate::switches::{Debouncer, DebounceResult};
use rbitset::BitSet128;

pub type Sink   = ErasedPin<Input<PullUp>>;
pub type Source = ErasedPin<Output<OpenDrain>>;


pub struct Matrix {
    sinks: Vec<Sink>,
    sources: Vec<Source>,
    pub raw_state: SmallBitVec,
    // pub debounced_state: SmallBitVec,
    debouncer: Debouncer,
}

impl Matrix {
    pub fn new(
        sinks: Vec<Sink>,
        sources: Vec<Source>,
    ) -> Matrix {
        let size = sinks.len() * sources.len();
        let raw_state = SmallBitVec::with_capacity(size);
        // let debounced_state = SmallBitVec::with_capacity(size);
        let debouncer = Debouncer::new(size);

        Matrix {
            sinks,
            sources,
            raw_state,
            // debounced_state,
            debouncer,
        }
    }

    pub fn len(&self) -> usize {
        return self.raw_state.capacity();
    }

    pub fn read_matrix(&mut self) -> Vec<(usize, DebounceResult)> {
        self.raw_state.clear();

        //should be useless, but better safe than sorry
        for source in self.sources.iter_mut() {
            source.set_high();
        }

        for source in self.sources.iter_mut() {
            source.set_low();
            Self::read_row(&mut self.raw_state, &self.sinks);
            source.set_high();
        }

        let mut debounce_events = Vec::new();
        for (switch_index, switch_state) in self.raw_state.iter().enumerate() {
            match self.debouncer.update(switch_index, switch_state) {
                d @ DebounceResult::Pressed | d @ DebounceResult::Released => {debounce_events.push((switch_index, d));},
                _ => {}
            }
        }
        return debounce_events;
    }

    pub fn get_debounced_state(&self) -> &BitSet128 {
        self.debouncer.pressed_state()
    }

    pub fn clear(&mut self) {
        // self.raw_state.clear();
        // self.debouncer.clear();

        // for _ in self.sources.iter() {
        //     for _ in self.sinks.iter() {
        //         self.raw_state.push(false);
        //     }
        // }
    }

    fn read_row(raw_state: &mut SmallBitVec, sinks: &Vec<Sink>) {
        //wait to be sure that source is low
        cortex_m::asm::delay(100);
        for sink in sinks.iter() {
            raw_state.push(sink.is_low());
        }
    }

}
