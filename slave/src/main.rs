/*
 * File: main.rs
 * Project: src
 * Created Date: Monday February 1st 2021
 * Author: Ronan (ronan.lashermes@0nline.fr)
 * -----
 * Last Modified: Tuesday, 14th June 2022 3:35:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021
 */


#![no_std]
#![no_main]

#![feature(default_alloc_error_handler)]


pub mod switches;
#[macro_use]
pub mod helpers;




extern crate alloc;
use alloc_cortex_m::CortexMHeap;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();



#[rtic::app(device = stm32f1xx_hal::pac)]
mod app {
    use crate::ALLOCATOR;

    use stm32f1xx_hal::{
        gpio::{gpioc::PC13, Output, PushPull, PinState},
        pac,
        prelude::*,
        timer::{CounterHz, Event},
        usb::{Peripheral, UsbBus, UsbBusType},
        serial::{Config, Serial},
        // time::{Hertz},
    };

    // you can put a breakpoint on `rust_begin_unwind` to catch panics
    use panic_halt as _;

    use rtt_target::rtt_init_default;
    use nb::block;
    use cortex_m::asm::delay;

    // use embedded_hal::digital::v2::OutputPin;


    use usb_device::bus;
    use usb_device::prelude::*;
    // use usb_device::class::UsbClass;
    use usbd_serial::{SerialPort};


    use no_std_compat::prelude::v1::*;

    use crate::{
        switches::{Matrix, DebounceResult},
    };


    use stm32f1xx_hal::gpio::{
        gpiob::{PB10, PB11},
        Alternate,
        Input,
        Floating,
    };

    const USB_REFRECH_FREQUENCY: u32 = 50;
    const KEYBOARD_UPDATE_FREQUENCY: u32 = 400;
    const LED_BLINK_FREQUENCY: u32 = 1;

    const READ_CMD: u8 = 0x55;
    const END_CMD:  u8 = 0xFF;

    const HEAP_SIZE: usize = 10 * 1024;

    fn init_allocator() {
        let start = cortex_m_rt::heap_start() as usize;
        unsafe { ALLOCATOR.init(start, HEAP_SIZE) }
    }
    
    #[shared]
    struct SharedResources {
        led:                    PC13<Output<PushPull>>,
        usb_timer:              CounterHz<pac::TIM1>,
        led_toggle_timer:       CounterHz<pac::TIM2>,
        keyboard_timer:         CounterHz<pac::TIM3>,
        usb_dev:                UsbDevice<'static, UsbBusType>,
        usb_serial:             SerialPort<'static, UsbBusType>,
        matrix:                 Matrix,
        serial:                 Serial<pac::USART3, (PB10<Alternate<PushPull>>, PB11<Input<Floating>>)>,

        led_state: bool,
    }

    #[local]
    struct LocalResources {

    }

    #[init]
    fn init(cx: init::Context) -> (SharedResources, LocalResources, init::Monotonics) {

        static mut USB_BUS: Option<bus::UsbBusAllocator<UsbBusType>> = None;

        rtt_init_default!();
        init_allocator();

        // Take ownership over the raw flash and rcc devices and convert them into the corresponding
        // HAL structs
        let mut flash = cx.device.FLASH.constrain();
        let rcc = cx.device.RCC.constrain();

        // Freeze the configuration of all the clocks in the system and store the frozen frequencies
        // in `clocks`
        let clocks = rcc
            .cfgr
            .use_hse(8.MHz())
            .sysclk(48.MHz())
            .pclk1(24.MHz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        // Acquire the GPIO peripherals
        let mut gpioa = cx.device.GPIOA.split();
        let mut gpiob = cx.device.GPIOB.split();
        let mut gpioc = cx.device.GPIOC.split();

        // BluePill board has a pull-up resistor on the D+ line.
        // Pull the D+ pin down to send a RESET condition to the USB bus.
        // This forced reset is needed only for development, without it host
        // will not reset your device when you upload new firmware.
        // let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        // usb_dp.set_low().unwrap();
        // delay(clocks.sysclk().0 / 100);

        let usb_dm = gpioa.pa11;
        let usb_dp = gpioa.pa12;

        let usb = Peripheral {
            usb: cx.device.USB,
            pin_dm: usb_dm,
            pin_dp: usb_dp,
        };


        unsafe {
            USB_BUS.replace(UsbBus::new(usb));
        }


        let usb_serial = SerialPort::new( unsafe { USB_BUS.as_ref().unwrap() });

        let usb_dev = UsbDeviceBuilder::new(unsafe { USB_BUS.as_ref().unwrap() }, UsbVidPid(0x16c0, 0x27dd))
            .manufacturer("claviconome")
            .product("claviconome_slave_debug")
            .serial_number("DEBUG")
            .device_class(usbd_serial::USB_CLASS_CDC)
            .build();

        // USART3
        // Configure pb10 as a push_pull output, this will be the tx pin
        let tx = gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh);
        // Take ownership over pb11
        let rx = gpiob.pb11;

        // Prepare the alternate function I/O registers
        let mut afio = cx.device.AFIO.constrain();

        // Set up the usart device. Taks ownership over the USART register and tx/rx pins. The rest of
        // the registers are used to enable and configure the device.
        let mut serial = Serial::usart3(
            cx.device.USART3,
            (tx, rx),
            &mut afio.mapr,
            Config::default().baudrate(230400.bps()),
            clocks,
        );



        for b in b"Debug welcome" {
            block!(serial.write(*b)).ok();
        }
        
        // Configure gpio C pin 13 as a push-pull output. The `crh` register is passed to the
        // function in order to configure the port. For pins 0-7, crl should be passed instead
        let led = gpioc
            .pc13
            .into_push_pull_output_with_state(&mut gpioc.crh, PinState::High);


        // Configure the syst timer to trigger an update every second and enables interrupt
        let mut timer = cx.device.TIM1.counter_hz(&clocks);
        timer.start(USB_REFRECH_FREQUENCY.Hz()).unwrap();
        timer.listen(Event::Update);

        let mut timer2 = cx.device.TIM2.counter_hz(&clocks);
        timer2.start(LED_BLINK_FREQUENCY.Hz()).unwrap();
        timer2.listen(Event::Update);

        let mut timer3 = cx.device.TIM3.counter_hz(&clocks);
        timer3.start(KEYBOARD_UPDATE_FREQUENCY.Hz()).unwrap();
        timer3.listen(Event::Update);

        //initialize the keypad matrix
        let matrix = Matrix::new(
            vec![
                gpiob.pb0.into_pull_up_input(&mut gpiob.crl).erase(), // green
                gpiob.pb1.into_pull_up_input(&mut gpiob.crl).erase(), // orange
                gpiob.pb5.into_pull_up_input(&mut gpiob.crl).erase(), // white
                gpiob.pb6.into_pull_up_input(&mut gpiob.crl).erase(), // red
                gpiob.pb7.into_pull_up_input(&mut gpiob.crl).erase(), // purple
                gpiob.pb8.into_pull_up_input(&mut gpiob.crh).erase(), // yellow
            ],
            vec![
                gpioa.pa0.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// black
                gpioa.pa1.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// blue
                gpioa.pa2.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// yellow
                gpioa.pa3.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// red
                gpioa.pa4.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// purple
                gpioa.pa5.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// white
                gpioa.pa6.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// green
            ]
        );

        //to keep track of time
        // let watch = Watch::new(1_000_000 / USB_REFRECH_FREQUENCY);



        // Init the static resources to use them later through RTIC
        let shared = SharedResources {
            led,
            usb_timer: timer,
            led_toggle_timer: timer2,
            keyboard_timer: timer3,
            usb_dev,
            usb_serial,
            // watch,
            matrix,
            serial,
            led_state: false,
        };

        let local = LocalResources {};
        return (shared, local, init::Monotonics());
    }

    // Optional.
    //
    // https://rtic.rs/0.5/book/en/by-example/app.html#idle
    // > When no idle function is declared, the runtime sets the SLEEPONEXIT bit and then
    // > sends the microcontroller to sleep after running init.
    // #[idle]
    // fn idle(_cx: idle::Context) -> ! {
    //     loop {
    //         cortex_m::asm::wfi();
    //     }
    // }

    #[task(binds = TIM1_UP, priority = 3, shared = [usb_timer, matrix, serial])]
    fn usb_timer_tick(cx: usb_timer_tick::Context) {

        let mut usb_timer = cx.shared.usb_timer;
        let mut matrix = cx.shared.matrix;
        let mut serial = cx.shared.serial;

        (&mut usb_timer, &mut matrix, &mut serial).lock(|usb_timer, matrix, serial|{
            let switch_pressed_positions = matrix.get_debounced_state();
            
            // if let Ok(byte_read) =  serial.read() {
            //     if byte_read == READ_CMD {
            //         for switch_pos in switch_pressed_positions.iter() {
            //             if switch_pressed_positions.contains(switch_pos) {
            //                 // send position
            //                 block!(serial.write(switch_pos as u8)).ok();
            //             }
            //         }

            //         // send end command
            //         block!(serial.write(END_CMD)).ok();
            //     }
            // }

            


            // block!(serial.write(READ_CMD)).ok();
            
            // for switch_pos in switch_pressed_positions.iter() {
            //     if switch_pressed_positions.contains(switch_pos) {
            //         // send position
            //         block!(serial.write(switch_pos as u8)).ok();
            //     }
            // }

            // // send end command
            // block!(serial.write(END_CMD)).ok();
                
            
            
            //prepare next tick
            usb_timer.start(USB_REFRECH_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            usb_timer.clear_interrupt(Event::Update);
        })
        
    }

    #[task(binds = TIM2, priority = 1, shared = [led, led_toggle_timer, led_state])]
    fn led_toggle_timer_tick(cx: led_toggle_timer_tick::Context) {
        let mut led = cx.shared.led;
        let mut led_toggle_timer = cx.shared.led_toggle_timer;
        let mut led_state = cx.shared.led_state;

        (&mut led, &mut led_toggle_timer, &mut led_state).lock(|led, led_toggle_timer, led_state| {

            if *led_state {
                led.set_high();
                *led_state = false;
            } else {
                led.set_low();
                *led_state = true;
            }

            //prepare next tick
            led_toggle_timer.start(LED_BLINK_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            led_toggle_timer.clear_interrupt(Event::Update);
        });
    }

    #[task(binds = TIM3, priority = 2, shared = [keyboard_timer, matrix, serial])]
    fn keyboard_timer_tick(cx: keyboard_timer_tick::Context) {

        let mut serial = cx.shared.serial;
        let mut keyboard_timer = cx.shared.keyboard_timer;
        let mut matrix = cx.shared.matrix;

        (&mut keyboard_timer, &mut matrix, &mut serial).lock(|keyboard_timer, matrix, serial|{
            let debounce_events = matrix.read_matrix();// update debouncer state

            for (pos, event) in debounce_events {
                let newpos = {
                    if event == DebounceResult::Pressed {
                        pos | 0x80
                    }
                    else {
                        pos
                    }
                };

                block!(serial.write(newpos as u8)).ok();

                // wait acknowledge
                loop {
                    delay(10);
                    if let Ok(byte_read) = serial.read() {
                        if byte_read == (newpos as u8) {
                            break;
                        }
                        else { // send again
                            block!(serial.write(newpos as u8)).ok();
                        }
                    }
                }
            }

            
            //prepare next tick
            keyboard_timer.start(KEYBOARD_UPDATE_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            keyboard_timer.clear_interrupt(Event::Update);
        })
        
    }

    #[task(binds = USB_HP_CAN_TX, shared = [usb_dev, usb_serial])]
    fn usb_tx(cx: usb_tx::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut usb_serial = cx.shared.usb_serial;

        (&mut usb_dev, &mut usb_serial).lock(|mut usb_dev, mut usb_serial| {
            usb_poll(&mut usb_dev, &mut usb_serial);
        });
    }

    #[task(binds = USB_LP_CAN_RX0, shared = [usb_dev, usb_serial])]
    fn usb_rx0(cx: usb_rx0::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut usb_serial = cx.shared.usb_serial;

        (&mut usb_dev, &mut usb_serial).lock(|mut usb_dev, mut usb_serial| {
            usb_poll(&mut usb_dev, &mut usb_serial);
        });
    }

    fn usb_poll<B: bus::UsbBus>(
        usb_dev: &mut UsbDevice<'static, B>,
        serial: &mut SerialPort<'static, B>,
    ) {
        if !usb_dev.poll(&mut [serial]) {
            return;
        }
    
        let mut buf = [0u8; 8];
    
        match serial.read(&mut buf) {
            // Ok(count) if count > 0 => {
            //     // Echo back in upper case
            //     for c in buf[0..count].iter_mut() {
            //         if 0x61 <= *c && *c <= 0x7a {
            //             *c &= !0x20;
            //         }
            //     }
    
            //     serial.write(&buf[0..count]).ok();
            // }
            _ => {}
        }
    }
}



