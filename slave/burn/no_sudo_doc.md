1 - Add user to plugdev group:

```
sudo useradd -G plugdev $(whoami)
```

2 - Copy file 99-openocd.rules in /etc/udev/rules.d/

3 - Restart udev.

```
sudo udevadm trigger
```
