/*
 * File: default.nix
 * Project: bluepills
 * Created Date: Thursday November 5th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 5th November 2020 1:58:19 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */
with import <nixpkgs> {};

let
  pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "my-old-revision";                                                 
         url = "https://github.com/NixOS/nixpkgs/";                       
         ref = "refs/heads/nixpkgs-unstable";                     
         rev = "860b56be91fb874d48e23a950815969a7b832fbc";                                           
     }) {};
in 

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "burner";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    openocd
  ];

  shellHook = ''
    export OCD_SCRIPTS_PATH=${openocd}/share/openocd/scripts
  '';

}