/*
 * File: main.rs
 * Project: src
 * Created Date: Monday February 1st 2021
 * Author: Ronan (ronan.lashermes@0nline.fr)
 * -----
 * Last Modified: Friday, 27th August 2021 8:21:57 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021
 */


#![no_std]
#![no_main]

#![feature(default_alloc_error_handler)]


pub mod km;
pub mod switches;
pub mod usb;
#[macro_use]
pub mod helpers;


// set_allocator!();

extern crate alloc;
use alloc_cortex_m::CortexMHeap;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();


// Generic keyboard from
// https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt




// #[app(device = stm32f1xx_hal::pac, peripherals = true)]
#[rtic::app(device = stm32f1xx_hal::pac)]
mod app {
    use crate::ALLOCATOR;

    use stm32f1xx_hal::{
        gpio::{gpioc::PC13, Output, PushPull, PinState},
        pac,
        prelude::*,
        timer::{CounterHz, Event},
        usb::{Peripheral, UsbBus, UsbBusType},
        spi::{Spi, FullDuplex},
        serial::{Config, Serial},
        // time::{Hertz},
    };

    // you can put a breakpoint on `rust_begin_unwind` to catch panics
    use panic_halt as _;

    use rtt_target::rtt_init_default;
    use nb::block;
    // use cortex_m::asm::delay;

    // use embedded_hal::digital::v2::OutputPin;


    use usb_device::bus;
    use usb_device::prelude::*;
    // use usb_device::class::UsbClass;
    // use usbd_serial::{SerialPort, USB_CLASS_CDC};

    use smart_leds::{SmartLedsWrite, RGB8};
    // use ws2812_spi as ws2812;
    use apa106_spi as ws2812;
    use ws2812::Ws2812;

    use no_std_compat::prelude::v1::*;

    use crate::{
        switches::{Matrix},
        usb::{KeyboardHidClass, HidClass, KbHidReport},
        km::{
            KeyboardManager,
            KmAction,
        }
    };

    use rbitset::BitSet128;


    //for the led
    use stm32f1xx_hal::spi::Spi2NoRemap;
    use stm32f1xx_hal::gpio::{
        gpiob::{PB10, PB11, PB13, PB14, PB15},
        Alternate,
        Input,
        Floating,
    };

    const VID: u16 = 0x27db;
    const PID: u16 = 0x16c0;
    const USB_REFRECH_FREQUENCY: u32 = 50;
    const KEYBOARD_UPDATE_FREQUENCY: u32 = 400;
    const LED_BLINK_FREQUENCY: u32 = 5;

    const READ_CMD: u8 = 0x55;
    const END_CMD:  u8 = 0xFF;

    const HEAP_SIZE: usize = 10 * 1024;


    fn init_allocator() {
        let start = cortex_m_rt::heap_start() as usize;
        unsafe { ALLOCATOR.init(start, HEAP_SIZE) }
    }

    #[shared]
    struct SharedResources { // -> Shared
        led:                PC13<Output<PushPull>>,
        usb_timer:          CounterHz<pac::TIM1>,
        led_toggle_timer:   CounterHz<pac::TIM2>,
        keyboard_timer:     CounterHz<pac::TIM3>,
        usb_dev:            UsbDevice<'static, UsbBusType>,
        matrix:             Matrix,
        km:                 KeyboardManager,
        rgb_led:            Ws2812<Spi<stm32f1xx_hal::pac::SPI2, Spi2NoRemap, (PB13<Alternate<PushPull>>, PB14<Input<Floating>>, PB15<Alternate<PushPull>>), u8>>,
        serial:             Serial<pac::USART3, (PB10<Alternate<PushPull>>, PB11<Input<Floating>>)>,
        right_hand:         BitSet128,

        mode_led_color: RGB8,
        caps_led_color: RGB8,
        led_state: bool,

    }

    #[local]
    struct LocalResources {

    }

    #[init]
    fn init(cx: init::Context) -> (SharedResources, LocalResources, init::Monotonics) { //(Shared, Local, Monotonics)
        static mut USB_BUS: Option<bus::UsbBusAllocator<UsbBusType>> = None;

        rtt_init_default!();
        init_allocator();

        // Take ownership over the raw flash and rcc devices and convert them into the corresponding
        // HAL structs
        let mut flash = cx.device.FLASH.constrain();
        let rcc = cx.device.RCC.constrain();

        // Freeze the configuration of all the clocks in the system and store the frozen frequencies
        // in `clocks`
        let clocks = rcc
            .cfgr
            .use_hse(8.MHz())
            .sysclk(72.MHz())
            .pclk1(36.MHz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        // Acquire the GPIO peripherals
        let mut gpioa = cx.device.GPIOA.split();
        let mut gpiob = cx.device.GPIOB.split();
        let mut gpioc = cx.device.GPIOC.split();

        // BluePill board has a pull-up resistor on the D+ line.
        // Pull the D+ pin down to send a RESET condition to the USB bus.
        // This forced reset is needed only for development, without it host
        // will not reset your device when you upload new firmware.
        // let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        // usb_dp.set_low().unwrap();
        // delay(clocks.sysclk().0 / 100);

        // let usb_dm = gpioa.pa11;
        // let usb_dp = usb_dp.into_floating_input(&mut gpioa.crh);


        let usb_dm = gpioa.pa11;
        let usb_dp = gpioa.pa12;

        let usb = Peripheral {
            usb: cx.device.USB,
            pin_dm: usb_dm,
            pin_dp: usb_dp,
        };

        unsafe {
            USB_BUS.replace(UsbBus::new(usb));
        }
        // USB_BUS = Some(UsbBus::new(usb));
        // let usb_bus = USB_BUS.as_ref().unwrap();

        // let usb_class: KeyboardHidClass = HidClass::new(KbHidReport::default(), &usb_bus);
        let usb_class: KeyboardHidClass = HidClass::new(KbHidReport::default(), unsafe { USB_BUS.as_ref().unwrap() });
        let usb_dev = UsbDeviceBuilder::new(unsafe { USB_BUS.as_ref().unwrap() }, UsbVidPid(VID, PID))
            .manufacturer("Artefaritaj")
            .product("Claviconome")
            .serial_number(env!("CARGO_PKG_VERSION"))
            .build();

        // USART3
        // Configure pb10 as a push_pull output, this will be the tx pin
        let tx = gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh);
        // Take ownership over pb11
        let rx = gpiob.pb11;

        // Prepare the alternate function I/O registers
        let mut afio = cx.device.AFIO.constrain();

        // Set up the usart device. Taks ownership over the USART register and tx/rx pins. The rest of
        // the registers are used to enable and configure the device.
        let serial = Serial::usart3(
            cx.device.USART3,
            (tx, rx),
            &mut afio.mapr,
            Config::default().baudrate(230400.bps()),
            clocks,
        );
        

        // Configure gpio C pin 13 as a push-pull output. The `crh` register is passed to the
        // function in order to configure the port. For pins 0-7, crl should be passed instead
        let led = gpioc
            .pc13
            .into_push_pull_output_with_state(&mut gpioc.crh, PinState::High);


        // Configure the syst timer to trigger an update every tick and enables interrupt
        let mut timer = cx.device.TIM1.counter_hz(&clocks);
        timer.start(USB_REFRECH_FREQUENCY.Hz()).unwrap();
        timer.listen(Event::Update);

        // let mut timer2 = Timer::tim2(cx.device.TIM2, &clocks).start_count_down(1.Hz());
        let mut timer2 = cx.device.TIM2.counter_hz(&clocks);
        timer2.start(LED_BLINK_FREQUENCY.Hz()).unwrap();
        timer2.listen(Event::Update);

        let mut timer3 = cx.device.TIM3.counter_hz(&clocks);
        timer3.start(KEYBOARD_UPDATE_FREQUENCY.Hz()).unwrap();
        timer3.listen(Event::Update);

        //initialize the keypad matrix
        let matrix = Matrix::new(
            vec![
                gpiob.pb0.into_pull_up_input(&mut gpiob.crl).erase(), // green
                gpiob.pb1.into_pull_up_input(&mut gpiob.crl).erase(), // orange
                // gpiob.pb5.into_pull_up_input(&mut gpiob.crl).erase(), // white //deficient on the chip
                gpioa.pa10.into_pull_up_input(&mut gpioa.crh).erase(),
                gpiob.pb6.into_pull_up_input(&mut gpiob.crl).erase(), // red
                gpiob.pb7.into_pull_up_input(&mut gpiob.crl).erase(), // purple
                gpiob.pb8.into_pull_up_input(&mut gpiob.crh).erase(), // yellow
            ],
            vec![
                gpioa.pa0.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// black
                gpioa.pa1.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// blue
                gpioa.pa2.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// yellow
                gpioa.pa3.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// red
                gpioa.pa4.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// purple
                gpioa.pa5.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// white
                gpioa.pa6.into_open_drain_output_with_state( &mut gpioa.crl, stm32f1xx_hal::gpio::PinState::High).erase(),// green
            ]
        );

        // initialize LED
        let pins = (
            gpiob.pb13.into_alternate_push_pull(&mut gpiob.crh),
            gpiob.pb14.into_floating_input(&mut gpiob.crh),
            gpiob.pb15.into_alternate_push_pull(&mut gpiob.crh),
        );
        let spi = Spi::spi2(cx.device.SPI2, pins, ws2812::MODE, 3.MHz(), clocks);
        // let spi = Spi::spi2(cx.device.SPI2, pins, ws2812::MODE, 2352.khz(), clocks, &mut rcc.apb1);
        let mut rgb_led = Ws2812::new(spi);
        

        //to keep track of time
        // let watch = Watch::new(1_000_000 / USB_REFRECH_FREQUENCY);

        let km = KeyboardManager::new(usb_class);


        // initial colors
        let grey_color = RGB8 { r: 0x0, g: 0x0, b: 0x0};
        set_led_colors(&mut rgb_led, grey_color, grey_color);


        // Init the static resources to use them later through RTIC
        let shared = SharedResources {
            led,
            usb_timer: timer,
            led_toggle_timer: timer2,
            keyboard_timer: timer3,
            usb_dev,
            matrix,
            km,
            rgb_led,
            serial,
            right_hand: BitSet128::new(),

            mode_led_color: grey_color,
            caps_led_color: grey_color,
            led_state: false,
        };

        let local = LocalResources {};

        return (shared, local, init::Monotonics());
    }

   
    #[task(binds = TIM2, priority = 1, shared = [led, led_toggle_timer, led_state/*, serial, right_hand*/])]
    fn led_toggle_timer_tick(cx: led_toggle_timer_tick::Context) {

        // let mut serial = cx.shared.serial;
        // let mut right_hand = cx.shared.right_hand;
        let mut led = cx.shared.led;
        let mut led_toggle_timer = cx.shared.led_toggle_timer;
        let mut led_state = cx.shared.led_state;

        (&mut led, &mut led_toggle_timer, &mut led_state/*, &mut serial, &mut right_hand*/).lock(|led, led_toggle_timer, led_state/*, serial, right_hand*/| {

            if *led_state {
                led.set_high();
                *led_state = false;
            } else {
                led.set_low();
                *led_state = true;
            }

            // right_hand.clear();

            // // send command to read right hand state
            // block!(serial.write(READ_CMD)).ok();

            // loop { // until end command receive (MUST HAVE RELIABLE CONNECTION)
            //     if let Ok(byte_read) = serial.read() {
            //         if byte_read == END_CMD {//exit condition
            //             break;
            //         }
            //         else {
            //             let new_pos = (byte_read as usize) + 42;
            //             right_hand.insert(new_pos);
            //         }
            //     }
            //     else {
            //         cortex_m::asm::delay(100);
            //     }
            // }

            //prepare next tick
            led_toggle_timer.start(LED_BLINK_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            led_toggle_timer.clear_interrupt(Event::Update);
        });
    }

    #[task(binds = TIM1_UP, priority = 2, shared = [usb_timer, matrix, rgb_led, km, mode_led_color, caps_led_color, right_hand])]
    fn usb_timer_tick(cx: usb_timer_tick::Context) {

        let mut usb_timer = cx.shared.usb_timer;
        let mut matrix = cx.shared.matrix;
        let mut rgb_led = cx.shared.rgb_led;
        let mut km = cx.shared.km;
        let mut mode_led_color = cx.shared.mode_led_color;
        let mut caps_led_color = cx.shared.caps_led_color;
        let mut right_hand = cx.shared.right_hand;

        //read local matrix
        (&mut usb_timer, &mut matrix, &mut rgb_led, &mut km, &mut mode_led_color, &mut caps_led_color, &mut right_hand).lock(|usb_timer, matrix, mut rgb_led, km, mode_led_color, caps_led_color, right_hand| {
            let switch_pressed_positions = matrix.get_debounced_state();
            
            let all_switches: BitSet128 = switch_pressed_positions.union(&right_hand).collect();

            let mut clear = false;

            //register the events
            
            match km.register_events(&all_switches) {
                Ok(ka) => {
                    match ka {
                        KmAction::ChangeLayer => {
                            clear = true;
                        }
                        _ => {}
                    }
                },
                Err(_) => {
                    *mode_led_color = RGB8 { r: 0xD0, g: 0, b: 0};
                    set_led_colors(&mut rgb_led, *mode_led_color, *caps_led_color);
                    // set_led_color(&mut cx.shared.rgb_led, RGB8 { r: 0xD0, g: 0, b: 0});
                }
            }
            

            match km.pop_send() {
                Ok(_) => {},
                Err(_) => {
                    *mode_led_color = RGB8 { r: 0xD0, g: 0xD0, b: 0};
                    set_led_colors(&mut rgb_led, *mode_led_color, *caps_led_color);
                    // set_led_color(&mut cx.shared.rgb_led, RGB8 { r: 0xD0, g: 0xD0, b: 0});
                }
            }

            //update the led color if needed
            if let Some(new_layer_led) = km.new_layer_led_color {
                *mode_led_color = new_layer_led;
                set_led_colors(&mut rgb_led, *mode_led_color, *caps_led_color);
                // set_led_color(&mut cx.shared.rgb_led, new_led);
            }
            km.new_layer_led_color = None;


            if clear {
                right_hand.clear();
                matrix.clear();
                km.clear();
            }

            

            //prepare next tick
            usb_timer.start(USB_REFRECH_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            usb_timer.clear_interrupt(Event::Update);
        });
    }

    #[task(binds = TIM3, priority = 3, shared = [keyboard_timer, matrix, serial, right_hand])]
    fn keyboard_timer_tick(cx: keyboard_timer_tick::Context) {

        
        let mut right_hand = cx.shared.right_hand;
        let mut serial = cx.shared.serial;
        let mut keyboard_timer = cx.shared.keyboard_timer;
        let mut matrix = cx.shared.matrix;

        (&mut keyboard_timer, &mut matrix, &mut serial, &mut right_hand).lock(|keyboard_timer, matrix, serial, right_hand|{

            if let Ok(byte_read) = serial.read() {
                if byte_read == READ_CMD {
                    right_hand.clear();
                    loop { // until end command receive (MUST HAVE RELIABLE CONNECTION)
                        if let Ok(byte_read) = serial.read() {
                            if byte_read == READ_CMD {
                                right_hand.clear();
                            }
                            if byte_read == END_CMD {//exit condition
                                break;
                            }
                            else {
                                let new_pos = (byte_read as usize) + 42;
                                right_hand.insert(new_pos);
                            }
                        }
                    }
                }
                else {
                    // send ack
                    block!(serial.write(byte_read as u8)).ok();
                    let event = byte_read >> 7;
                    let new_pos = ((byte_read & 0x7F) as usize) + 42;
        
                    if event == 1 {
                        right_hand.insert(new_pos);
                    }
                    else {
                        right_hand.remove(new_pos);
                    }
                }
            }

            matrix.read_matrix();// update left hand debouncer state
            
            //prepare next tick
            keyboard_timer.start(KEYBOARD_UPDATE_FREQUENCY.Hz()).unwrap();

            // Clears the update flag
            keyboard_timer.clear_interrupt(Event::Update);
        })
        
    }

    #[task(binds = USB_HP_CAN_TX, shared = [usb_dev, km])]
    fn usb_tx(cx: usb_tx::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut km = cx.shared.km;

        (&mut usb_dev, &mut km).lock(|mut usb_dev, km| {
            km.report_buffer_mut().usb_poll(&mut usb_dev);
        });
        // cx.shared.km.report_buffer_mut().usb_poll(&mut cx.shared.usb_dev);
    }

    #[task(binds = USB_LP_CAN_RX0, shared = [usb_dev, km])]
    fn usb_rx0(cx: usb_rx0::Context) {
        let mut usb_dev = cx.shared.usb_dev;
        let mut km = cx.shared.km;

        (&mut usb_dev, &mut km).lock(|mut usb_dev, km| { 
            km.report_buffer_mut().usb_poll(&mut usb_dev);
        });
        // cx.shared.km.report_buffer_mut().usb_poll(&mut cx.shared.usb_dev);
    }

    // #[task(binds = USART3, resources = [serial, km, rgb_led])]
    // fn usart3(mut cx: usart3::Context) {

    //     set_led_color(&mut cx.shared.rgb_led, RGB8 { r: 0x20, g: 0, b: 0});

    //     let mut events: Vec<MatrixKeyEvent> = Vec::with_capacity(4);
    //     while let Ok(byte_read) =  cx.shared.serial.read() {
    //             let switch_index = (byte_read & 0x7F) as usize;
    //             let event_kind = if (byte_read | 0x80) != 0 {
    //                 KeyEvent::Pressed
    //             }
    //             else {
    //                 KeyEvent::Released
    //             };
    //             events.push(MatrixKeyEvent { switch_index, event: event_kind });
    //     }

    //     if events.len() > 0 {
    //         cx.shared.km.register_events(events).unwrap();
    //     }
    // }

    fn set_led_colors<SPI: FullDuplex<u8> >(driver: &mut Ws2812<SPI>, mode_color: RGB8, caps_color: RGB8) 
    where SPI::Error: core::fmt::Debug {
        let mut led_state: [RGB8; 2] = [RGB8::default(); 2];
        led_state[0] = mode_color;
        led_state[1] = caps_color;
        driver.write(led_state.iter().cloned()).unwrap();
    }
}



