/*
 * File: layer.rs
 * Project: km
 * Created Date: Monday February 8th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 8th February 2021 6:04:22 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

/// A layer is a mapping between a physical key and a unicode character.
#[derive(Debug)]
pub struct Layer {

}