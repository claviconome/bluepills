/*
 * File: keyboard_manager.rs
 * Project: km
 * Created Date: Monday February 8th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 23rd February 2021 10:35:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::usb::ReportBuffer;
// use crate::Watch;
use crate::km::codes::*;
use crate::km::layouts::Layer;
use alloc::boxed::Box;
use crate::km::layouts::physical::dactyl_claviconome::DactylClaviconome;
use crate::km::keyboard_output::KeyboardOutput;

use smart_leds::RGB8;

use alloc::{vec, vec::Vec};
use crate::km::layouts::layered::{
    text::Text,
    navigation::Navigation,
};

use crate::usb::KeyboardHidClass;

use rbitset::BitSet128;

pub struct KeyboardManager {
    report_buffer:              ReportBuffer,
    pub new_layer_led_color:    Option<RGB8>, // Some(color) if the led color must be updated, None otherwise.
    current_layer_ind:          LayerId,
    current_connection_status:  bool,
    layers:                     Vec<Box<dyn Layer + Send>>,
    layer_switching_active:     bool, // true if layer was switch recently (key still active)
    physical:                   DactylClaviconome,
}

#[derive(Debug, Clone, Copy, PartialEq,Eq)]
pub enum KmAction {
    ChangeLayer,
    Keys,
    None
}

impl KeyboardManager {
    pub fn new(usb_class: KeyboardHidClass) -> KeyboardManager {
        let physical = DactylClaviconome::new();
        let report_buffer = ReportBuffer::new(usb_class);
        
        let layer_0: Box<dyn Layer + Send> = Box::new(Text {});
        let layer_1: Box<dyn Layer + Send> = Box::new(Navigation {});
        let new_layer_led_color = Some(RGB8 { r: 0x5, g: 0, b: 0 }); //Some(layer_0.color());

        let layers  = vec![layer_0, layer_1];

        KeyboardManager {
            report_buffer,
            new_layer_led_color,
            current_layer_ind: LayerId(0),
            current_connection_status: false,
            layers,
            layer_switching_active: false,
            physical,
        }
    }
    
    pub fn pop_send(&mut self) -> Result<(), &str> {
        self.report_buffer.pop_send()
    }

    pub fn register_events(&mut self, switch_pressed_positions: &BitSet128) -> Result<KmAction, ()> {

            // for event in matrix_events {
            //     let event_kind      = event.event;
            //     let switch_index    = event.switch_index;
            //     // physical to positional (wiring independant)
            //     let positional     = self.physical.switch_index_to_positional(switch_index)?;
            //     let pos = positional.into_usize();

            //     match event_kind {
            //         KeyEvent::Pressed => {
            //             self.pressed_positions.insert(pos);
            //         },
            //         KeyEvent::Released => {
            //             self.pressed_positions.remove(pos);
            //         }
            //     }
            // }



        //select a layer
        if self.current_layer_ind.into_usize() < self.layers.len() {
            let mut usages: Vec<UsageId> = Vec::new();
            //prepare a variable to switch to a new layer
            let mut next_layer_ind = self.current_layer_ind;

            // for each positional, do an action (or not)
            
            let mut layout_codes: Vec<LayoutCode> = Vec::new();
            for switch_pos in switch_pressed_positions.iter() {
                if switch_pressed_positions.contains(switch_pos) {
                    let pos = self.physical.switch_index_to_positional(switch_pos)?;
                    let mut new_lcode = self.layers[self.current_layer_ind.into_usize()].positional_to_layout_codes(pos.into());
                    layout_codes.append(&mut new_lcode);
                }
            }
            
            let mut change_layer = false;
            if self.layer_switching_active == true {
                if layout_codes.len() == 0 {
                    self.layer_switching_active = false;
                }
            }

            for layout_code in layout_codes {
                match layout_code {
                    LayoutCode::UsageId(uid)    => { usages.push(uid); },
                    LayoutCode::ToLayer(l)      => { 
                        if self.layer_switching_active == false {
                            next_layer_ind = l; 
                            change_layer = true;
                            self.layer_switching_active = true;
                        }
                    },
                    LayoutCode::DeadKey         => {},
                }
            }

            let new_connection_status = self.report_buffer.connection_initiated();
            

            // //update layer index
            if self.current_layer_ind != next_layer_ind || new_connection_status != self.current_connection_status {
                self.current_layer_ind = next_layer_ind;
                self.current_connection_status = new_connection_status;
                self.new_layer_led_color = Some(self.layers[next_layer_ind.into_usize()].color());
            }
            

            // //usages id are registered in report buffer
            // self.report_buffer.queue_keys(&usages);
            let new_keys = match self.report_buffer.send_keys(&usages) {
                Err(_) => {
                    return Err(());
                },
                Ok(b) => { b }
            };

            
            if change_layer {
                return Ok(KmAction::ChangeLayer);
            }
            else if new_keys {
                return Ok(KmAction::Keys);
            }
            else {
                return Ok(KmAction::None);
            }            
        }
        else {//reset layer but ignore this event
            self.current_layer_ind = LayerId(0);
            return Err(());
        }

        
    }

    pub fn clear(&mut self){
        self.report_buffer.clear();
    }

    pub fn report_buffer_mut(&mut self) -> &mut ReportBuffer {
        &mut self.report_buffer
    }

}