/*
 * File: positional_code.sr
 * Project: km
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 10th February 2021 2:06:01 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


/// This code represent a physical position on the keyboard, independant of software.
///[ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 
/// 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 
/// 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 
/// 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
/// 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
/// 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
/// 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83]
/// Used as an index for final layouts
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct PositionalCode(u8);


impl PositionalCode {
    pub fn new(code: u8) -> PositionalCode {
        PositionalCode(code)
    }

    pub fn into_u8(self) -> u8 {
        self.0
    }

    pub fn into_usize(self) -> usize {
        self.0 as usize
    }
}

impl From<usize> for PositionalCode {
    fn from(u: usize) -> PositionalCode {
        PositionalCode((u & 0xFF) as u8)
    }
}