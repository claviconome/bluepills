/*
 * File: usageid.rs
 * Project: km
 * Created Date: Monday February 8th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 10:10:13 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

// use crate::km::*;
// use core::convert::TryFrom;

/// Represent a usage ID to fill the usb report
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct UsageId(pub u16);

impl UsageId {
    pub const NO: UsageId               = UsageId(0x00);
    pub const ERROR_ROLLOVER: UsageId   = UsageId(0x01);
    pub const POST_FAIL: UsageId        = UsageId(0x02);
    pub const ERROR_UNDEFINED: UsageId  = UsageId(0x03);

    pub const LCTRL: UsageId            = UsageId(0xE0);
    pub const LSHIFT: UsageId           = UsageId(0xE1);
    pub const LALT: UsageId             = UsageId(0xE2);
    pub const LGUI: UsageId             = UsageId(0xE3);
    pub const RCTRL: UsageId            = UsageId(0xE4);
    pub const RSHIFT: UsageId           = UsageId(0xE5);
    pub const RALT: UsageId             = UsageId(0xE6);
    pub const RGUI: UsageId             = UsageId(0xE7);

    /// needed to build USB reports
    pub fn is_modifier(self) -> bool {
        match self {
            UsageId::LCTRL | UsageId::LSHIFT | UsageId::LALT | UsageId::LGUI |
            UsageId::RCTRL | UsageId::RSHIFT | UsageId::RALT | UsageId::RGUI => true,
            _ => false
        }
    }
    /// needed to build USB reports
    pub fn as_modifier_bit(self) -> u8 {
        if self.is_modifier() {
            1 << (self.0 - UsageId::LCTRL.0) as u8
        } else {
            0
        }
    }

    pub fn into_u8(self) -> u8 {
        self.into()
    }
}

impl From<UsageId> for u8 {
    fn from(uid: UsageId) -> u8 {
        (uid.0 & 0xFF) as u8
    }
}

impl From<u8> for UsageId {
    fn from(u: u8) -> UsageId {
        UsageId(u as u16)
    }
}

impl From<u16> for UsageId {
    fn from(u: u16) -> UsageId {
        UsageId(u)
    }
}


//Useless

// const AT101_TO_USAGEID: [u8; 60] = 
//             [0x35, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26,//10
//              0x27, 0x2D, 0x2E, 0x00, 0x2A, 0x2B, 0x14, 0x1A, 0x08, 0x15,//20
//              0x17, 0x1C, 0x18, 0x0C, 0x12, 0x13, 0x2F, 0x30, 0x31, 0x39,//30
//              0x04, 0x16, 0x07, 0x09, 0x0A, 0x0B, 0x0D, 0x0E, 0x0F, 0x33,//40
//              0x34, 0x32, 0x28, 0xE1, 0x64, 0x1D, 0x1B, 0x06, 0x19, 0x05,//50
//              0x11, 0x10, 0x36, 0x37, 0x38, 0x87, 0xE5, 0xE0, 0x00, 0xE2,//60 
//              ];

// impl TryFrom<At101Position> for UsageId {
//     type Error = &'static str;

//     fn try_from(pos: At101Position) -> Result<Self, Self::Error> {
//         match pos {

//             _ => Err("Cannot convert AT-101 position to usage ID.")
//         }
//     }
// }