/*
 * File: layer_id.rs
 * Project: codes
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 10:15:37 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct LayerId(pub u8);


impl From<LayerId> for usize {
    fn from(id: LayerId) -> usize {
        id.0 as usize
    }
}

impl LayerId {
    pub fn into_usize(self) -> usize {
        self.0 as usize
    }

    pub fn into_u8(self) -> u8 {
        self.0
    }
}