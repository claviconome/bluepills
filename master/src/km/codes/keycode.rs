/*
 * File: keycode.rs
 * Project: km
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 5:22:50 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum Modifier {
    LeftControl,
    LeftShift,
    LeftAlt,
    LeftGUI,
    RightControl,
    RightShift,
    RightAlt,
    RightGUI
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum Function {
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12
}


#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum Keycode {
    Char(char),
    Modifier(Modifier),
    Function(Function),
}

impl From<Function> for Keycode {
    fn from(f: Function) -> Keycode {
        Keycode::Function(f)
    }
}

impl From<char> for Keycode {
    fn from(c: char) -> Keycode {
        Keycode::Char(c)
    }
}

impl From<Modifier> for Keycode {
    fn from(modifier: Modifier) -> Keycode {
        Keycode::Modifier(modifier)
    }
}