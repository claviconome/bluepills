/*
 * File: layout_code.rs
 * Project: layered
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 9:50:02 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::codes::*;

 #[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum LayoutCode {
    UsageId(UsageId),
    ToLayer(LayerId),
    DeadKey
}

impl From<u16> for LayoutCode {
    fn from(c: u16) -> LayoutCode {
        match c {
            0xFF => LayoutCode::DeadKey,
            u => LayoutCode::UsageId(u.into())
        }
        
    }
}