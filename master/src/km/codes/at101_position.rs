/*
 * File: at101_position.rs
 * Project: km
 * Created Date: Monday February 8th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 8th February 2021 8:59:47 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

/// Represent a position on AT-101 keyboard.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct At101Position (u8);

impl At101Position {
    pub fn new(pos: u8) -> Result<At101Position, ()> {
        match pos {
            p @ 1..=58      => Ok(At101Position(p)),
            p @ 60..=62     => Ok(At101Position(p)),
            p @ 64          => Ok(At101Position(p)),
            p @ 75..=76     => Ok(At101Position(p)),
            p @ 79..=81     => Ok(At101Position(p)),
            p @ 83..=86     => Ok(At101Position(p)),
            p @ 89..=110    => Ok(At101Position(p)),
            p @ 111..=126   => Ok(At101Position(p)),
            _ => Err(())
        }
    }
}