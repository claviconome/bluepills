/*
 * File: keyboard_output.rs
 * Project: km
 * Created Date: Friday February 5th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 23rd February 2021 10:22:17 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::UsageId;


pub trait KeyboardOutput {

    // /// Send these key codes concurrently at some point in time;
    // fn queue_keys(&mut self, keys: &[UsageId]);

    /// Send these key codes concurrently right away. Queue is operation fails.
    fn send_keys(&mut self, keys: &[UsageId]) -> Result<bool, &str>;

    /// helper that sends an empty status
    fn send_empty(&mut self) -> Result<bool, &str>{
        let empty: [UsageId; 0] = [UsageId::NO; 0]; 
        self.send_keys(&empty)
    }

    // fn queue_empty(&mut self) {
    //     let empty: [UsageId; 0] = [UsageId::NO; 0]; 
    //     self.queue_keys(&empty);
    // }
}