/*
 * File: layer.rs
 * Project: layered
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 10th February 2021 2:00:29 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use alloc::vec::Vec;
use crate::km::codes::*;
use smart_leds::RGB8;

pub trait Layer {
    fn positional_to_layout_codes(&mut self, pos: PositionalCode) -> Vec<LayoutCode>;
    fn color(&self) -> RGB8;
}