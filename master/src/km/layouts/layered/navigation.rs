/*
 * File: text.rs
 * Project: final
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 15th February 2021 5:38:23 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


use crate::km::codes::*;
use alloc::{vec::Vec, vec};
use crate::km::layouts::Layer;
use smart_leds::RGB8;

pub struct Navigation {}

unsafe impl Send for Navigation {}

impl Layer for Navigation {
    fn positional_to_layout_codes(&mut self, pos: PositionalCode) -> Vec<LayoutCode> {

        let deadkey = LayoutCode::DeadKey;
        let text = LayoutCode::ToLayer(LayerId(0));

        let mapping: [LayoutCode; 84] = [
                deadkey,     deadkey,     deadkey,     deadkey,     deadkey, 0x81.into(), 0x80.into(),     deadkey,     deadkey, 0x46.into(), 0x47.into(), 0x48.into(),
                deadkey,     deadkey,     deadkey,     deadkey,     deadkey,     deadkey,     deadkey, 0x5F.into(), 0x60.into(), 0x61.into(),     deadkey,     deadkey,
            0x2B.into(),     deadkey, 0x4A.into(), 0x52.into(), 0x4D.into(), 0x4B.into(),     deadkey, 0x5C.into(), 0x5D.into(), 0x5E.into(),     deadkey,     deadkey,
                deadkey,     deadkey, 0x50.into(), 0x51.into(), 0x4F.into(), 0x4E.into(),     deadkey, 0x59.into(), 0x5A.into(), 0x5B.into(),     deadkey,     deadkey,
                deadkey,     deadkey,    deadkey,      deadkey,     deadkey, 0x49.into(),     deadkey, 0x62.into(),     deadkey, 0x58.into(),     deadkey,     deadkey,
                deadkey,     deadkey,     deadkey,        text, 0xE3.into(), 0x29.into(), 0x2A.into(), 0x4C.into(), 0x2E.into(), 0x30.into(),     deadkey,     deadkey,
                deadkey,     deadkey, 0x2C.into(), 0xE2.into(), 0xE0.into(), 0xE1.into(), 0xE5.into(), 0xE4.into(), 0xE6.into(), 0x28.into(),     deadkey,     deadkey,
            ];

        let pos_index = pos.into_usize();
        if pos_index < mapping.len() {
            return vec![mapping[pos_index]];
        }
        else {
            return Vec::new();
        }
    }

    fn color(&self) -> RGB8 {
        RGB8 { r: 0, g: 0x5, b: 0 }
    }
}
