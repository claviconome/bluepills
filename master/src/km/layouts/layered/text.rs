/*
 * File: bepo.rs
 * Project: final
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 12th February 2021 5:03:34 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


use crate::km::codes::*;
use alloc::{vec::Vec, vec};
use crate::km::layouts::Layer;
use smart_leds::RGB8;

pub struct Text {}

unsafe impl Send for Text {}

impl Layer for Text {
    fn positional_to_layout_codes(&mut self, pos: PositionalCode) -> Vec<LayoutCode> {

        let deadkey = LayoutCode::DeadKey;
        let navigation = LayoutCode::ToLayer(LayerId(1));

        let mapping: [LayoutCode; 84] = [
            0x3A.into(), 0x3B.into(), 0x3C.into(), 0x3D.into(), 0x3E.into(), 0x3F.into(), 0x40.into(), 0x41.into(), 0x42.into(), 0x43.into(), 0x44.into(), 0x45.into(),
            0x35.into(), 0x1E.into(), 0x1F.into(), 0x20.into(), 0x21.into(), 0x22.into(), 0x23.into(), 0x24.into(), 0x25.into(), 0x26.into(), 0x27.into(), 0x2D.into(),
            0x2B.into(), 0x14.into(), 0x1A.into(), 0x08.into(), 0x15.into(), 0x17.into(), 0x1C.into(), 0x18.into(), 0x0C.into(), 0x12.into(), 0x13.into(), 0x2F.into(),
            0x39.into(), 0x04.into(), 0x16.into(), 0x07.into(), 0x09.into(), 0x0A.into(), 0x0B.into(), 0x0D.into(), 0x0E.into(), 0x0F.into(), 0x33.into(), 0x34.into(),
            0x64.into(), 0x1D.into(), 0x1B.into(), 0x06.into(), 0x19.into(), 0x05.into(), 0x11.into(), 0x10.into(), 0x36.into(), 0x37.into(), 0x38.into(), 0x32.into(),
                deadkey,     deadkey,     deadkey,  navigation, 0xE3.into(), 0x29.into(), 0x2A.into(), 0x4C.into(), 0x2E.into(), 0x30.into(),     deadkey,     deadkey,
                deadkey,     deadkey, 0x2C.into(), 0xE2.into(), 0xE0.into(), 0xE1.into(), 0xE5.into(), 0xE4.into(), 0xE6.into(), 0x28.into(),     deadkey,     deadkey,
            ];

        let pos_index = pos.into_usize();
        if pos_index < mapping.len() {
            return vec![mapping[pos_index]];
        }
        else {
            return Vec::new();
        }
    }

    fn color(&self) -> RGB8 {
        RGB8 { r: 0, g: 0, b: 0x5 }
    }
}
