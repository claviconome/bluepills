/*
 * File: dactyl_claviconome.rs
 * Project: physical
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 11th February 2021 3:54:45 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::codes::PositionalCode;

const MAX_SWITCH_INDEX: usize = 84;

const DACTYL_CLAVICONOME: [u8; 84] =   [36,   37, 40, 41, 38, 39, 81, 80, 83, 82,   79,   78,
                                        30,   31, 34, 35, 32, 33, 75, 74, 77, 76,   73,   72,
                                        24,   25, 28, 29, 26, 27, 69, 68, 71, 70,   67,   66,
                                        18,   19, 22, 23, 20, 21, 63, 62, 65, 64,   61,   60,
                                        12,   13, 16, 17, 14, 15, 57, 56, 59, 58,   55,   54,
                                      0xFF, 0xFF, 10, 11,  8,  9, 51, 50, 53, 52, 0xFF, 0xFF,
                                      0xFF, 0xFF,  2,  3,  7,  6, 48, 49, 45, 44, 0xFF, 0xFF
                                    ];

pub struct DactylClaviconome {
    reverse_mapping: [u8; MAX_SWITCH_INDEX]
}

impl DactylClaviconome {
    pub fn new() -> DactylClaviconome {
        let mut reverse_mapping: [u8; MAX_SWITCH_INDEX as usize] = [0xFF; MAX_SWITCH_INDEX as usize];

        for i in 0..DACTYL_CLAVICONOME.len() {
            if (DACTYL_CLAVICONOME[i] as usize) < MAX_SWITCH_INDEX {
                reverse_mapping[DACTYL_CLAVICONOME[i] as usize] = i as u8;
            }
        }

        DactylClaviconome { reverse_mapping }
    }

    /// Transform the switch index from the matrix event to a positional code.
    pub fn switch_index_to_positional(&self, switch_index: usize) -> Result<PositionalCode, ()> {
        
        if switch_index < MAX_SWITCH_INDEX {
            let pos = self.reverse_mapping[switch_index];

            if pos == 0xFF {
                return Err(())
            }
            else {
                return Ok(PositionalCode::new(pos));
            }
        }
        else {
            return Err(());
        }
    }
}

