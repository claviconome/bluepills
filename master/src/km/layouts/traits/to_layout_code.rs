/*
 * File: to_layoutcode.rs
 * Project: traits
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 7:56:13 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::codes::{LayoutCode, PositionalCode};

pub trait ToLayoutCode {
    fn to_layout_code(&self, pos_code: PositionalCode) -> Result<LayoutCode, ()>;
}