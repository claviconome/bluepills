/*
 * File: to_keycode.rs
 * Project: traits
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 7:54:59 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::codes::{Keycode, PositionalCode};

pub trait ToKeycode {
   fn to_keycode(&self, pos_code: PositionalCode) -> Result<Keycode, ()>;
}