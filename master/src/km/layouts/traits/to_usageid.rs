/*
 * File: keycode_to_usageid.rs
 * Project: os
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 4:40:57 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::km::UsageId;
use alloc::vec::Vec;

pub trait ToUsageId {
    fn to_usage_id(&self) -> Vec<UsageId>;
}