/*
 * File: physical_to_positional.rs
 * Project: physical
 * Created Date: Tuesday February 9th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 9th February 2021 4:49:56 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */
use crate::km::codes::PositionalCode;

pub trait ToPositional {
    fn to_positional(&self, switch_index: u8) -> Result<PositionalCode, ()>;
}