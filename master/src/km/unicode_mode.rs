/*
 * File: unicode_mode.rs
 * Project: km
 * Created Date: Friday February 5th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 5th February 2021 7:38:20 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[derive(Clone,Debug,PartialEq,Eq,Copy)]
pub enum UnicodeMode {
    Linux,
    WinCompose
}