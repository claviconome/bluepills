/*
 * File: keyboard_mode.rs
 * Project: km
 * Created Date: Monday February 8th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 8th February 2021 6:09:41 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum KeyboardMode {
    /// In direct mode, a physical key pressed is a set (possibly empty) of KeyCode pressed.
    Direct,

    /// In macro mode, upon validation, the macro command is erased and replaced by the macro content.
    Macro,
}