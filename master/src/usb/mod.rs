/*
 * File: mod.rs
 * Project: usb
 * Created Date: Thursday February 4th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 5th February 2021 7:45:31 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

pub mod hid;
pub mod report_buffer;
pub mod kb_hid_report;

pub use self::hid::{HidDevice, HidClass};
pub use self::kb_hid_report::KbHidReport;

use stm32f1xx_hal::usb::UsbBusType;

pub type KeyboardHidClass = HidClass<'static, UsbBusType, KbHidReport>;
pub use self::report_buffer::ReportBuffer;