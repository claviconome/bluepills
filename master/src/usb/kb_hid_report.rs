/*
 * File: kbhidreport.rs
 * Project: usb
 * Created Date: Friday February 5th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 14th June 2022 2:44:20 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

// use crate::km::KeyCode;
use crate::km::UsageId;

#[derive(Default, Clone, PartialEq)]
pub struct KbHidReport {
    report: [u8; 8]
}

impl KbHidReport {
    pub fn as_bytes(&self) -> &[u8] {
        &self.report
    }

    pub fn pressed(&mut self, kc: UsageId) {
        match kc {
            UsageId::NO => {},
            UsageId::ERROR_ROLLOVER | UsageId::POST_FAIL | UsageId::ERROR_UNDEFINED => self.set_all(kc),
            kc if kc.is_modifier() => self.report[0] |= kc.as_modifier_bit(),
            _ => self.report[2..]
                .iter_mut()
                .find(|c| **c == 0)
                .map(|c| *c = kc.into())
                .unwrap_or_else(|| self.set_all(UsageId::ERROR_ROLLOVER)),
        }
    }

    pub fn released(&mut self, kc: UsageId) {
        match kc {
            UsageId::NO => {},
            UsageId::ERROR_ROLLOVER | UsageId::POST_FAIL | UsageId::ERROR_UNDEFINED  => self.set_all(UsageId::NO),
            kc if kc.is_modifier() => self.report[0] &= !kc.as_modifier_bit(),
            _ => self.report[2..]
                .iter_mut()
                .find(|c| **c == kc.into())
                .map(|c| *c = 0)
                .unwrap_or_else(|| self.set_all(UsageId::ERROR_ROLLOVER)),
        }
    }

    fn set_all(&mut self, kc: UsageId) {
        for c in &mut self.report[2..] {
            *c = kc.into();
        }
    }

    pub fn clear(&mut self) {
        for c in &mut self.report {
            *c = 0;
        }
    }
}

use crate::usb::hid::{HidDevice, Protocol, ReportType, Subclass};


const REPORT_DESCRIPTOR: &[u8] = &[
    // 0x05, 0x01,    // UsagePage(Generic Desktop[1])
    // 0x09, 0x06,    // UsageId(Keyboard[6])
    // 0xA1, 0x01,    // Collection(Application)
    // 0x85, 0x01,    //     ReportId(1)
    // 0x05, 0x07,    //     UsagePage(Keyboard/Keypad[7])
    // 0x19, 0xE0,    //     UsageIdMin(Keyboard LeftControl[224])
    // 0x29, 0xE7,    //     UsageIdMax(Keyboard Right GUI[231])
    // 0x15, 0x00,    //     LogicalMinimum(0)
    // 0x25, 0x01,    //     LogicalMaximum(1)
    // 0x95, 0x08,    //     ReportCount(8)
    // 0x75, 0x01,    //     ReportSize(1)
    // 0x81, 0x02,    //     Input(Data, Variable, Absolute, NoWrap, Linear, PreferredState, NoNullPosition, BitField)
    // 0x19, 0x01,    //     UsageIdMin(ErrorRollOver[1])
    // 0x29, 0x65,    //     UsageIdMax(Keyboard Application[101])
    // 0x15, 0x01,    //     LogicalMinimum(1)
    // 0x25, 0x65,    //     LogicalMaximum(101)
    // 0x75, 0x07,    //     ReportSize(7)
    // 0x81, 0x00,    //     Input(Data, Array, Absolute, NoWrap, Linear, PreferredState, NoNullPosition, BitField)
    // 0x05, 0x08,    //     UsagePage(LED[8])
    // 0x19, 0x01,    //     UsageIdMin(Num Lock[1])
    // 0x29, 0x05,    //     UsageIdMax(Kana[5])
    // 0x15, 0x00,    //     LogicalMinimum(0)
    // 0x25, 0x01,    //     LogicalMaximum(1)
    // 0x95, 0x05,    //     ReportCount(5)
    // 0x75, 0x01,    //     ReportSize(1)
    // 0x91, 0x02,    //     Output(Data, Variable, Absolute, NoWrap, Linear, PreferredState, NoNullPosition, NonVolatile, BitField)
    // 0x95, 0x01,    //     ReportCount(1)
    // 0x75, 0x03,    //     ReportSize(3)
    // 0x91, 0x03,    //     Output(Constant, Variable, Absolute, NoWrap, Linear, PreferredState, NoNullPosition, NonVolatile, BitField)
    // 0xC0,          // EndCollection()

    // 0x05, 0x01, // Usage Page (Generic Desktop),
    // 0x09, 0x06, // Usage (Keyboard),
    // 0xA1, 0x01, // Collection (Application),
    //     0x05, 0x07, //   Usage Page (Key Codes),
    //     0x19, 0xE0, //   Usage Minimum (224),
    //     0x29, 0xE7, //   Usage Maximum (231),
    //     0x15, 0x00, //   Logical Minimum (0),
    //     0x25, 0x01, //   Logical Maximum (1),
    //     0x75, 0x01, //   Report Size (1),
    //     0x95, 0x08, //   Report Count (8)
    //     0x81, 0x02, //   Input (Data, Variable, Absolute), ;Modifier keys
    //     0x95, 0x01, //   Report Count (1)
    //     0x75, 0x08, //   Report Size (8)
    //     0x81, 0x03, //   Input (Constant),          ;Reserved byte
    //     0x95, 0x05, //   Report Count (5)
    //     0x75, 0x01, //   Report Size (1)
    //     0x05, 0x08, //   Usage Page (LEDs)
    //     0x19, 0x01, //   Usage Minimum (1)
    //     0x29, 0x05, //   Usage Maximum (5)
    //     0x91, 0x02, //   Output (Data, Variable, Absolute), ;LED report
    //     0x95, 0x01, //   Report Count (1),
    //     0x75, 0x03, //   Report Size (3),
    //     0x91, 0x03, //   Output (Constant),         ;LED report padding
    //     0x95, 0x06, //   Report Count (6)
    //     0x75, 0x08, //   Report Size (8)
    //     0x15, 0x00, //   Logical Minimum (0)
    //     0x25, 0x65, //   Logical Maximum(101)
    //     0x05, 0x07, //   Usage Page (Key Codes),
    //     0x19, 0x00, //   Usage Minimum (0),
    //     0x29, 0x65, //   Usage Maximum (101),
    //     0x81, 0x00, //   Input (Data, Array),       ;Normal keys
    //     // 0x09, 0x03,
    //     // 0x75, 0x08, //   Report Size (8)
    //     // 0x95, 0x40, //   Report Count (64)
    //     // 0xB1, 0x02, //   Feature (variable,absolute)
    // 0xC0, // End Collection

    0x05, 0x01, // Usage Page (Generic Desktop),
    0x09, 0x06, // Usage (Keyboard),
    0xA1, 0x01, // Collection (Application),
        0x05, 0x07, //   Usage Page (Key Codes),
        0x19, 0xE0, //   Usage Minimum (224),
        0x29, 0xE7, //   Usage Maximum (231),
        0x15, 0x00, //   Logical Minimum (0),
        0x25, 0x01, //   Logical Maximum (1),
        0x75, 0x01, //   Report Size (1),
        0x95, 0x08, //   Report Count (8)
        // 0x09, 0x06, // Usage (Keyboard),
        0x81, 0x02, //   Input (Data, Variable, Absolute), ;Modifier keys
        0x95, 0x01, //   Report Count (1)
        0x75, 0x08, //   Report Size (8)
        // 0x09, 0x06, // Usage (Keyboard),
        0x81, 0x03, //   Input (Constant),          ;Reserved byte
        0x95, 0x05, //   Report Count (5)
        0x75, 0x01, //   Report Size (1)
        0x05, 0x08, //   Usage Page (LEDs)
        0x19, 0x01, //   Usage Minimum (1)
        0x29, 0x05, //   Usage Maximum (5)
        0x91, 0x02, //   Output (Data, Variable, Absolute), ;LED report
        0x95, 0x01, //   Report Count (1),
        0x75, 0x03, //   Report Size (3),
        0x91, 0x03, //   Output (Constant),         ;LED report padding
        0x95, 0x06, //   Report Count (6)
        0x75, 0x08, //   Report Size (8)
        0x15, 0x00, //   Logical Minimum (0)
        0x25, 0x65, //   Logical Maximum(101)
        0x05, 0x07, //   Usage Page (Key Codes),
        0x19, 0x00, //   Usage Minimum (0),
        0x29, 0x65, //   Usage Maximum (101),
        // 0x09, 0x06, // Usage (Keyboard),
        0x81, 0x00, //   Input (Data, Array),       ;Normal keys
        // 0x09, 0x03,
        // 0x75, 0x08, //   Report Size (8)
        // 0x95, 0x40, //   Report Count (64)
        // 0xB1, 0x02, //   Feature (variable,absolute)
    0xC0, // End Collection


];

impl HidDevice for KbHidReport {
    fn subclass(&self) -> Subclass {
        Subclass::BootInterface
    }

    fn protocol(&self) -> Protocol {
        Protocol::Keyboard
    }

    fn report_descriptor(&self) -> &[u8] {
        REPORT_DESCRIPTOR
    }

    fn get_report(&mut self, report_type: ReportType, _report_id: u8) -> Result<&[u8], ()> {
        match report_type {
            ReportType::Input => Ok(&self.report),
            _ => Err(()),
        }
    }

    fn set_report(
        &mut self,
        report_type: ReportType,
        report_id: u8,
        data: &[u8],
    ) -> Result<(), ()> {
        if report_type == ReportType::Output && report_id == 0 && data.len() == 1 {
            return Ok(());
        }
        else {
            return Err(())
        }
        
    }
}
