
use crate::usb::{KeyboardHidClass, KbHidReport};
use stm32f1xx_hal::usb::UsbBusType;
use crate::km::{UsageId, KeyboardOutput};
use usb_device::prelude::UsbDevice;

use usb_device::class::UsbClass;

// use core::clone::Clone;
// use keytokey::{KeyCode, KeyboardState, USBKeyOut};
use no_std_compat::collections::VecDeque;
use no_std_compat::prelude::v1::*;

// use crate::km::KeyCode;

#[allow(dead_code)]

pub struct ReportBuffer {
    pub usb_class: KeyboardHidClass,
    // current_report: KbHidReport,
    last_report: KbHidReport,
    buffer: VecDeque<KbHidReport>,
}

unsafe impl Sync for ReportBuffer {}

impl ReportBuffer {
    pub fn new(usb_class: KeyboardHidClass) -> ReportBuffer {
        ReportBuffer {
            usb_class,
            // current_report: KbHidReport::default(),
            last_report: KbHidReport::default(),
            buffer: VecDeque::with_capacity(4),
        }
    }

    pub fn connection_initiated(&self) -> bool {
        self.usb_class.connection_initiated()
    }

    pub fn pop_send(&mut self) -> Result<(), &str> {
        if let Some(report) = self.buffer.pop_front() {
            match self.usb_class.write(report.as_bytes()) {
                Ok(0) => { //try again?
                    self.buffer.push_front(report); // presumably doesn't happen?
                }
                Ok(_i) => {}, //complete report, presumably
                Err(_) => {
                    return Err("Error on send.");
                },
            }
        }

        Ok(())
    }

    pub fn clear(&mut self) {
        self.buffer.clear();
        match self.send_empty() {
            Err(_) => {},
            Ok(_) => {}
        }
    }


    pub fn usb_poll(&mut self, usb_dev: &mut UsbDevice<'static, UsbBusType>) {
        if usb_dev.poll(&mut [&mut self.usb_class]) {
            self.usb_class.poll();
        }
    }
}


impl KeyboardOutput for ReportBuffer {
    fn send_keys(&mut self, keys: &[UsageId]) -> Result<bool, &str> {
        let mut report = KbHidReport::default();
        for k in keys {
            report.pressed(*k);
        }
        // self.send_report(report)
        if report != self.last_report {
            self.buffer.push_back(report.clone());
            self.last_report = report;
            Ok(true)
        }
        else {
            Ok(false)
        }
        
    }

}