/*
 * File: watch.rs
 * Project: helpers
 * Created Date: Friday February 5th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 5th February 2021 1:53:55 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */


#[derive(PartialEq,Debug,Clone)]
// To keep track of absolute time.
pub struct Watch {
    microseconds: u64,
    microseconds_per_tick: u32,
}

impl Watch {
    pub fn new(microseconds_per_tick: u32) -> Watch {
        Watch { 
            microseconds: 0,
            microseconds_per_tick
        }
    }

    pub fn tick(&mut self) {
        self.microseconds += self.microseconds_per_tick as u64;
    }

    pub fn microseconds(&self) -> u64 {
        self.microseconds
    }

    pub fn milliseconds(&self) -> u64 {
        self.microseconds / 1_000
    }

    pub fn seconds(&self) -> u64 {
        self.microseconds / 1_000_000
    }
}