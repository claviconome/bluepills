// from https://gitlab.com/polymer-kb/firmware/polymer/-/blob/master/src/allocator.rs

// macro_rules! set_allocator {
//     () => {
//         extern crate alloc;
//         // use alloc::alloc::Layout;

//         use alloc_cortex_m::CortexMHeap;

//         const HEAP_SIZE: usize = 10 * 1024;

//         #[global_allocator]
//         static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

//         // #![feature(default_alloc_error_handler)]
//         // #[alloc_error_handler]
//         // pub fn rust_oom(_: Layout) -> ! {
//         //     loop {}
//         // }

//         fn init_allocator() {
//             let start = cortex_m_rt::heap_start() as usize;
//             unsafe { ALLOCATOR.init(start, HEAP_SIZE) }
//         }
//     };
// }
