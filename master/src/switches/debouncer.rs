/*
 * File: debouncer.rs
 * Project: src
 * Created Date: Wednesday February 3rd 2021
 * Author: Ronan ( )
 * -----
 * Last Modified: Tuesday, 23rd February 2021 9:46:13 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021  
 */


use rbitset::BitSet128;
use no_std_compat::prelude::v1::*;

pub struct Debouncer {
    patterns:       Vec<u8>,
    pressed_state:  BitSet128
}

// #[repr(u8)]
// #[derive(PartialEq)]
// pub enum DebounceResult {
//     NoChange,
//     Pressed,
//     Released
// }

impl Debouncer {
    pub fn new(no_of_keys: usize) -> Debouncer {
        Debouncer{
            patterns:       vec![0; no_of_keys],
            pressed_state:  BitSet128::new(),
        }
    }

    pub fn update(&mut self, key_no: usize, pressed: bool)
    {
        let next: u8 = if pressed {1} else {0};
        self.patterns[key_no] = self.patterns[key_no] << 1 | next;
        //debounce following hackadays ultimate debouncing schema
        let mask: u8 = 0b11000111;
        let this_pattern = self.patterns[key_no];
        let seen = this_pattern & mask;
        if seen == 0b00000111 {
            self.patterns[key_no] = 0b1111111;
            self.pressed_state.insert(key_no);
        }
        else if seen == 0b11000000 {
            self.patterns[key_no] = 0b0000000;
            self.pressed_state.remove(key_no);
        }
        else {
            if this_pattern == 0b1111111 && !self.pressed_state.contains(key_no) {
                self.pressed_state.insert(key_no);
            }
            else if this_pattern == 0b0000000 && self.pressed_state.contains(key_no) {
                self.pressed_state.remove(key_no);
            }
        }
    }

    pub fn pressed_state(&self) -> &BitSet128 {
        &self.pressed_state
    }

    pub fn pressed_state_mut(&mut self) -> &mut BitSet128 {
        &mut self.pressed_state
    }

    // Return list of keys to release
    pub fn clear(&mut self) -> Vec<usize> {

        let mut res = Vec::new();
        for i in 0..self.patterns.len() {
            if self.pressed_state.contains(i) {
                res.push(i);
                self.pressed_state.remove(i);
                self.patterns[i] = 0b0000000;
            }
        }

        return res;
    }
}