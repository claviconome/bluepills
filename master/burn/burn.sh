#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ -z "$1" ]
    then
        openocd -f $OCD_SCRIPTS_PATH/interface/stlink-v2.cfg -f $OCD_SCRIPTS_PATH/target/stm32f1x.cfg -c "program $SCRIPTPATH/claviconome_master verify reset exit"
    else
        openocd -f $OCD_SCRIPTS_PATH/interface/stlink-v2.cfg -f $OCD_SCRIPTS_PATH/target/stm32f1x.cfg -c "program $1 verify reset exit"
fi