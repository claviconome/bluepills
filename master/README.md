# K2k_stm32f103

A rust keyboard firmware for stm32f103 based microcontroller boards like the Bluepill.


You need to rustup target add thumbv7m-none-eabi to be able to compile:

'''bash
rustup target add thumbv7m-none-eabi
'''
